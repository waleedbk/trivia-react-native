import { Navigation } from  'react-native-navigation' 
import RegisterComponent from './src/config/RegisterComponent'
import { Provider, connect } from "react-redux";
import Store from "./src/Store";

RegisterComponent(Store); // To get all the registered components
    
Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setDefaultOptions({
    topBar: {
        visible: false,
        drawBehind: true
    }
  }),
  Navigation.setRoot({
    root: {
      stack: {
        id: "App",
        children: [
          {
            component: {
                name: "LoadingComponent",
                // name: "HomeComponent",
                // name: "SettingComponent",
                //     options: {
                //         topBar: {
                //             visible: true,
                //             drawBehind: false,
                //             title: {
                //                 text: "Settings",
                //                 fontSize: 20,
                //                 fontWeight: "bold"
                //             },
                //             rightButtons: [
                //                 {        
                //                     id: 'buttonOne',
                //                     icon: require('./public/images/logout-button.png'),
                //                     text: 'Button one',
                //                     enabled: true,
                //                     disableIconTint: false,
                //                     color: 'black',
                //                     disabledColor: 'black',
                //                 }
                //             ]
                //         }
                //     }
              // name: "LeaderboardComponent",
              // options: {
              //   topBar: {
              //       visible: true,
              //       drawBehind: false,
              //       title: {
              //           text: "Leaderboard",
              //           fontSize: 20,
              //           fontWeight: "bold"
              //       }
              //   }
              // }
            }
          }
        ],
      }
    }
  });
});