import { Navigation } from  "react-native-navigation";
import { FETCH_CURRENT_GAME, AUTH_USER_FULFILLED, FETCH_LEADERBOARD_FULFILLED, SET_VERIFICATION_ID, AUTH_USER_ANONY_FULFILLED } from "../const/UserConst";

const initialState = {
    authData: {
        verificationId: ""
    },
    userProfile: {},
    leaderboard: {},
    isNewUser: false,
    isFetching: true,
    error: false,
}

function UserReducer(state = initialState, action) {
    switch(action.type) {
        case "AUTH_USER": {
            console.warn("reached auth_user");
            return  Navigation.push ("App", {
                    component: {
                    name: "LoginComponent",
                    options: {
                        topBar: {
                          visible: false,
                          drawBehind: true
                        }
                      }
                    }
                } )
        }

        case SET_VERIFICATION_ID: {
            return {
                ...state,
                authData: {
                    ...state.authData,
                    verificationId: action.payload
                }
            }
        }

        case AUTH_USER_FULFILLED: {
            return {
                ...state,
                userProfile: {
                    // isNewUser: action.payload.additionalUserInfo.isNewUser,
                    uid: action.payload._user.uid
                }
            }
        }

        case AUTH_USER_ANONY_FULFILLED: {
            return {
                ...state,
                userProfile: {
                    uid: action.payload.user.uid
                }
            }
        }

        case FETCH_CURRENT_GAME: {
            return {
                ...state,
                leaderboard: action.payload
            }
        }


        case FETCH_LEADERBOARD_FULFILLED: {
            return {
                ...state,
                leaderboard: action.payload
            }
        }

        default:
            return state
    }
}

export default UserReducer;