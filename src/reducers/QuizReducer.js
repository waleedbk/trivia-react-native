import { Navigation } from  "react-native-navigation";
import { FETCH_CURRENT_GAME_FULLFILLED, SELECT_OPTION, INC_CURRENT_INDEX, START_GAME, RESET_GAME_STATE, SHOW_ANSWER, END_GAME } from "../const/QuizConst";
import { STOP_PLUS } from "../const/AnimationConst";

const initialStates = {
    /**  holding the current quiz fetched from database */

    currentGame: {
        isLive: false,
        startTime: "Not Schedule Yet",
        showResults: false,
        countDown: 10,
        game: {
            gamePrice: "",
            startTime: 0
        },
    },

    isGameStarted: 0, // 0 => not started, 1 => started
    selectedOption: [],
    pointsScored: 0,
    progress: 0,

    animation: {
        type: "",
        play: false
    }
};

function QuizReducer (state = initialStates, action) 
{
    switch (action.type) 
    {
        case FETCH_CURRENT_GAME_FULLFILLED: {
            return {
                ...state,
                currentGame: {
                    ...state.currentGame,
                    ...action.payload
                }
            }
        }

        case START_GAME: {
            return {
                ...state,
                isGameStarted: 1,
                selectedOption: [],
                pointsScored: 0,
                progress: 0,
            }
        }

        /***************************** 0 => optionIndex, 1 => confidenceLevel, 2 => skip, 3 => pointsGained on this option  *******************************/
        case SELECT_OPTION: {
            if (state.selectedOption[ state.currentGame.currentIndex ] != null && state.selectedOption[ state.currentGame.currentIndex ][2] != 1) 
            {
                if (state.selectedOption[ state.currentGame.currentIndex ][0] == action.payload.optionIndex )
                {
                    return {
                        ...state,
                        selectedOption: {
                            ...state.selectedOption,
                            [ state.currentGame.currentIndex ]: [ action.payload.optionIndex, action.payload.confidenceLevel, 0 ]
                        }
                    }    
                }
                return state;
            } else
            {
                return {
                    ...state,
                    selectedOption: {
                        ...state.selectedOption,
                        [ state.currentGame.currentIndex ]: [ action.payload.optionIndex, action.payload.confidenceLevel, 0 ]
                    }
                }
            }
        }

        case SHOW_ANSWER: {
            if(state.selectedOption[ state.currentGame.currentIndex ] != null && state.selectedOption[ state.currentGame.currentIndex ][2] != 1 && state.selectedOption[ state.currentGame.currentIndex ][3] == null) 
            {
                /*************************** Adding and subtracting points **********************/
                if (state.selectedOption[ state.currentGame.currentIndex ][0] == state.currentGame.game.quiz[ state.currentGame.currentIndex ].correct)
                {
                    state.selectedOption[ state.currentGame.currentIndex ][3] = state.selectedOption[ state.currentGame.currentIndex ][1];
                    return {
                        ...state,
                        pointsScored: state.pointsScored + state.selectedOption[ state.currentGame.currentIndex ][1],
                        progress: (state.pointsScored + state.selectedOption[ state.currentGame.currentIndex ][1]) / state.currentGame.game.totalPoints,
                        animation: {
                            type: "progressPlus",
                            play: true,
                        }
                    }
                } else
                {
                    state.selectedOption[ state.currentGame.currentIndex ][3] = -state.selectedOption[ state.currentGame.currentIndex ][1];
                    return {
                        ...state,
                        pointsScored: state.pointsScored - state.selectedOption[ state.currentGame.currentIndex ][1],
                        progress: (state.pointsScored - state.selectedOption[ state.currentGame.currentIndex ][1]) / state.currentGame.game.totalPoints,
                    }
                }
            } else
            {
                return {
                    ...state,
                }
            }
        }

        case STOP_PLUS: {
            return {
                ...state,
                animation: {
                    play: false
                }
            }
        }

        case INC_CURRENT_INDEX: {
            return {
                ...state,
                currentGame: {
                    ...state.currentGame,
                    currentQuizIndex: state.currentGame.currentIndex+1
                },
                selectedOption: {
                    ...state.selectedOption,
                    [ state.currentGame.currentIndex+1 ]: [ 0, 0, 0, 0 ]
                }
            }
        }

        case END_GAME: {
            return {
                ...state,
                isGameStarted: 0,
                currentGame: {
                    ...state.currentGame,
                    isLive: 0,
                }
            }
        }

        case RESET_GAME_STATE: {
            return {
                ...state,

            }
        }

        default:
            return state
    }
}

export default QuizReducer;