import { combineReducers } from 'redux';
import UserReducer from './UserReducer';
import QuizReducer from './QuizReducer';

const rootReducer = combineReducers ({
    UserReducer,
    QuizReducer
    // nav: navReducer,
});

export default rootReducer; 