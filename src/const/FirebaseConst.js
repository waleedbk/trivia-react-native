import firebase from 'react-native-firebase';

export const FIREBASE_DB = firebase.database();
export const FIREBASE_USER = firebase.database().ref("/users/");
export const NEXT_GAME = firebase.database().ref("/nextGame");
export const CURRENT_QUIZ = firebase.database().ref("/currentQuiz");
export const LEADERBOARD_REF = firebase.database().ref("leaderboard").orderByChild("totalScore").limitToFirst(50);