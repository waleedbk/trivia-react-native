import firebase from "react-native-firebase";
import { SELECT_OPTION, INC_CURRENT_INDEX, START_GAME, RESET_GAME_STATE, SHOW_ANSWER, END_GAME } from "../const/QuizConst"

export function StartGame (uid) 
{
    const playersRef = firebase.database().ref("/currentGame/players/"+uid);
    playersRef.update ({
        isPlaying: true
    })

    return ( dispatch ) =>
    {
        dispatch ({
            type: START_GAME,
        }) 
    }    
}

export function SelectOption (optionIndex, confidenceLevel) 
{
    return (dispatch) =>
    {
        dispatch ({
            type: SELECT_OPTION,
            payload: { optionIndex, confidenceLevel }
        })
    }
}

export function ShowAnswer (value) 
{
    return (dispatch, getState) => 
    {    
        dispatch ({
            type: SHOW_ANSWER,
            payload: value
        })

    }
}

export function IncCurrentIndex ()
{
    return (dispatch) => 
    {
        dispatch ({
            type: INC_CURRENT_INDEX,
        })
    }
}

export function ResetGameState ()
{
    return ( dispatch ) =>
    {
        dispatch ({
            type: RESET_GAME_STATE
        })
    }
}

export function EndGame () 
{
    return ( dispatch ) =>
    {
        dispatch ({
            type: END_GAME,
        }) 
    }    
}