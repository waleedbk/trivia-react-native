import firebase from "react-native-firebase";

import { SetHomeComponent, SetLoginComponent, PushVerifyComponent } from "../config/Navigation";

import { AUTH_USER_FULFILLED, AUTH_USER_ANONY_FULFILLED, FETCH_CURRENT_GAME,FETCH_CURRENT_GAME_FULLFILLED, AUTH_USER, FETCH_LEADERBOARD_FULFILLED, SET_VERIFICATION_ID } from "../const/UserConst";
import { CURRENT_QUIZ, FIREBASE_USER, FIREBASE_DB, LEADERBOARD_REF } from "../const/FirebaseConst";
import { SHOW_ANSWER } from "../const/QuizConst";
import { STOP_PLUS } from "../const/AnimationConst";


export function AuthenticateUser ()
{
    return (dispatch) =>
    {
        firebase.auth ()
        .onAuthStateChanged (user =>
        {
            console.warn ("auth changed", user);
            if (user == null) 
            {
                console.warn ("new user", user);
                SetLoginComponent ();
            } else
            { 
                console.warn ("authenticate user else", user)
                dispatch ({
                    type: AUTH_USER_FULFILLED,
                    payload: user
                })

                SetHomeComponent ();
            }
        });
    }
}

export function AuthenticateNumber (number)
{
    console.warn ("authenticate number", number);
    return (dispatch) =>
    {
        firebase.auth ()
        .verifyPhoneNumber (number)
        .on ("state_changed", (phoneAuthSnapshot) =>
        {
            console.log ("phone auth state",  phoneAuthSnapshot);
            switch (phoneAuthSnapshot.state)
            {
                // - if this is on IOS then phoneAuthSnapshot.code will always be null
                case firebase.auth.PhoneAuthState.CODE_SENT:
                    console.log("cond has been sent" );
                    if (phoneAuthSnapshot.code == null)
                    {

                        console.log("code null" );
                        dispatch ({
                            type: SET_VERIFICATION_ID,
                            payload: phoneAuthSnapshot.verificationId
                        })
                        PushVerifyComponent ();
                    }
                    break;

                case firebase.auth.PhoneAuthState.AUTO_VERIFY_TIMEOUT:
                    console.warn ("auto verified timeout");
                    dispatch ({
                        type: SET_VERIFICATION_ID,
                        payload: phoneAuthSnapshot.verificationId
                    });
                    // PushVerifyComponent ();
                    break;

                case firebase.auth.PhoneAuthState.AUTO_VERIFIED:
                    console.warn ("auto verified done");

                    const { verificationId, code } = phoneAuthSnapshot;

                    SignIn (verificationId, code);

                    break;

            }
        },
        ( error => 
        {
            console.warn("error authWithPhoneNumber", error);
        }))
    }
}

export function FetchNextGame ()
{
    return (dispatch, getState) =>
    {
        console.warn("fetch next game called")
        const currentQuizRef = firebase.database().ref("/currentGame/");
        currentQuizRef.on ("value" , (snapshot) =>
        {
            console.warn("next game value", snapshot);
            const showAnswer = snapshot.child("showAnswer").val();

            if (showAnswer == "true")
            {
                dispatch ({
                    type: SHOW_ANSWER,
                })
            } else if (showAnswer == "false")
            {
                dispatch ({
                    type: STOP_PLUS
                })
            }

            dispatch ({
                type: FETCH_CURRENT_GAME_FULLFILLED, // calling quiz reducer
                payload: snapshot.val (),
            })

            const pointsScored = getState ().QuizReducer.pointsScored;
            const uid = getState ().UserReducer.userProfile.uid;

            const playersRef = firebase.database ().ref ("/currentGame/players/"+uid);
            playersRef.update ({
                pointsScored: pointsScored
            })
        });
    }
}

export function FetchLeaderboard () 
{
    console.warn ("fetching leaderboard ")
    return (dispatch) =>
    {
        const leaderbaordData = {};
        LEADERBOARD_REF.on ("value", (snapshot) => 
        {
            if (snapshot.hasChildren)
            {
                snapshot.forEach ((childSnapshot) =>
                {
                    leaderbaordData[childSnapshot.key] = childSnapshot.val ();
                })

                dispatch ({
                    type: FETCH_LEADERBOARD_FULFILLED,
                    payload: leaderbaordData
                })
            }
        })
    }
}

/** Sign in user with credential after verfing phone number */
export function SignIn (verificationId, code) 
{
    return () =>
    {
        console.warn ("sign in", verificationId, code);
        const credential = firebase.auth.PhoneAuthProvider.credential (verificationId, code);
        firebase.auth ().signInWithCredential (credential);
    }
}

export function SignInAnonymously ()
{
    return (dispatch) =>
    {
        firebase.auth ()
        .signInAnonymously ()
        .then (credential => {

            if (credential)
            {
                console.warn ("user is logged in now", credential);
                const userRef = FIREBASE_DB.ref ("/users/"+credential.user.uid);

                userRef.update ({
                    online: true
                })

                dispatch ({
                    type: AUTH_USER_ANONY_FULFILLED,
                    payload: credential
                })
            }
        })
    }
}

export function Logout ()
{
    return () =>
    {
        firebase.auth ().signOut ().then( () => {
            console.warn ("sign out successfull ");
            // login screen 
            // SetLoginComponent (); 
        }).catch ((error) => 
        {
            console.warn ("erorr on signing out ", error);
        })
    }
}