import React, {Component} from 'react';
import {BackHandler, ActivityIndicator, Platform, Styleheet, View, TextInput, TouchableOpacity, ImageBackground, Alert, ScrollView} from 'react-native';
import {Container, Content, Button, Badge, Text, List, ListItem, Icon as NBaseIcon, Thumbnail, Right, Switch} from 'native-base';
import Style from '../assets/Style';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions/UserAction';
import Spinner from 'react-native-spinkit';
import CodeInput from 'react-native-confirmation-code-input';
import { Navigation } from 'react-native-navigation';

class SettingComponent extends Component {  
    static options ()
    {
        return {
          topBar: {
            rightButtons: [
                {
                    id: 'logout',
                    text: "Logout",
                    icon: require('../../public/images/logout-button.png'),
                    
                }
            ],
            visible: true,
            drawBehind: true
          },
        };
    }

    constructor (props)
    {
        super(props);

        this.state = {
            number: ""
        }
    }


    authenticateUser = (code) => {
        console.warn("component id: ",this.props.componentId);
        return Navigation.setStackRoot (this.props.componentId, {
            component: {
                name: 'HomeComponent',
                passProps: {
                    text: 'Root screen'
                },
                options: {
                    animations: {
                        setStackRoot: {
                        enable: true
                        }
                    }
                }
            }
          });
    }

    render() {
        return (
            <View style={[ Style.flex, Style.paddingDefault, Style.bgWhite ]}> 
                
                <View style={[ Style.alignCenter, Style.padding15 ]}>
                    <TouchableOpacity style={[ ]}>
                        <Thumbnail large source={require('../../public/images/avatar.png')} />
                        <NBaseIcon name='ios-camera' style={[ Style.textSecondary, Style.floatRight, {marginTop: -25} ]} />
                    </TouchableOpacity>
                    <Text style={[ Style.textBold, Style.textPrimary, Style.f20, Style.paddingH25, Style.paddingV5, Style.borderRadius20 ]}>John Doe</Text>
                </View>
                
                <Text style={Style.divider}></Text>
                
                <View style={[ Style.flex, Style.justifyCenter ]}>
                    <View style={[ Style.marginV5 ]}>
                        <Text style={[ Style.marginV5, Style.textBold, Style.textPrimary ]}>Notifications</Text>
                        <Text style={[ Style.marginV5, Style.textSecondary ]}>Get notified when the shows are live:</Text>
                        <View style={[ Style.marginV5, Style.paddingDefault, Style.bgLight, Style.borderRadius5, Style.flexRow, Style.spaceBetween]}>
                            <Text style={[ Style.textBold, Style.textPrimary ]}>Notification</Text>          
                            <Switch value={true} />
                        </View>
                    </View>
                    <View style={[ Style.marginV5 ]}>
                        <View style={[ Style.paddingDefault, Style.borderRadius5, Style.flexRow, Style.spaceBetween]}>
                            <Text style={[ Style.textBold, Style.textPrimary ]}>App audio</Text>          
                            <Switch value={false} />
                        </View>
                    </View>
                    <View style={[ Style.marginV5 ]}>
                        <View style={[ Style.paddingDefault, Style.borderRadius5, Style.flexRow, Style.spaceBetween ]}>
                            <Text style={[ Style.textBold, Style.textPrimary ]}>Quiz audio</Text>
                            <Switch value={false} />
                        </View>
                    </View>
                </View>

            </View>
        );
    }
}
function mapStateToProps(state, props) {
    return {
        userReducer: state.UserReducer
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch)
}
  
export default connect(mapStateToProps, mapDispatchToProps)(SettingComponent);