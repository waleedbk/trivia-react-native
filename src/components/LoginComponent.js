import React, {Component} from 'react';
import {BackHandler, ActivityIndicator, Platform, Styleheet, View, TextInput, TouchableOpacity, ImageBackground, Alert, ScrollView} from 'react-native';
import {Container, Content, Button, Badge, Text, List, ListItem, Icon as NBaseIcon, Thumbnail, Right} from 'native-base';
import Style from '../assets/Style';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions/UserAction';
import Spinner from 'react-native-spinkit';
import { Navigation } from "react-native-navigation";

class LoginComponent extends Component {  
    
    constructor (props)
    {
        super(props);
        this.state = {
            number: "+92"
        }
    }

    authenticateAnnonymously = () => 
    {
        console.warn("authenticate")
        Navigation.push ("App", {
            component: {
                name: "VerifyComponent",
                options: {
                    topBar: {
                        title: {
                            text: "Enter verification code",
                        },
                        visible: true,
                        drawBehind: false,
                        background: {
                            color: 'transparent',
                            translucent: true,
                            blur: false,
                        },
                        backButton: {
                            visible: true
                        }
                    }
                }
            }
        } )
    }

    authenticateNumber = () =>
    {
        this.props.AuthenticateNumber (this.state.number);
    }

    render() {
        return (
            <View style={[ Style.flex, Style.paddingDefault ]}> 

                <View style={[Style.flex]}>
                </View>

                <View style={[Style.flex, Style.justifyCenter, Style.alignCenter]}>
                    
                    <View style={[Style.flexRow, Style.paddingDefault, Style.contentCenter]}>
                        <Text style={[Style.textWhite, Style.textBold, Style.f20]}>
                            +92
                        </Text>
                        <TextInput keyboardType='numeric' maxLength={13} underlineColorAndroid="#FFFFFF" style={[ Style.flex, Style.marginH15, Style.f20, Style.textWhite ]} onChangeText={ (number) => this.setState({ number: number }) } value={this.state.number} />
                    </View>       
                    <View style={ Style.paddingV5 }>
                        <TouchableOpacity style={[ Style.button, Style.bgPurple ]} onPress={() => this.authenticateNumber()}>
                            <Text style={[ Style.textWhite, Style.textBold ]}>
                                Verify Phone Number
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={Style.paddingV5}>
                      <ActivityIndicator size="large" color="#FFFFFF" />
                    </View>
                    <Text style={[ Style.textWhite, Style.textCenter, Style.paddingV5 ]}>OR</Text>
                    <View style={Style.paddingV5}>
                        <TouchableOpacity onPress={() => this.authenticateAnnonymously()}>   
                            <Text style={[ Style.textWhite, Style.textCenter ]}>Sign in annonymously</Text> 
                        </TouchableOpacity>
                    </View>

                </View>

                <View style={[ Style.flex, Style.justifyBottom ]}>      
                    <Text style={[ Style.padding15, Style.borderRadius10, Style.bgWhite, Style.textPrimary, Style.textBold, Style.textCenter ]}>
                        By tapping "Verify Phone Number" an SMS will be send on your mobile phone with code for activiting account.    
                    </Text>
                </View>
                
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        userReducer: state.UserReducer
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch)
}
  
export default connect(mapStateToProps, mapDispatchToProps)(LoginComponent);