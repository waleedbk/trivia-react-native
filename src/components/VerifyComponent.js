import React, {Component} from 'react';
import { BackHandler, ActivityIndicator, Platform, Styleheet, View, TextInput, TouchableOpacity, ImageBackground, Alert, ScrollView } from 'react-native';
import { Container, Content, Button, Badge, Text, List, ListItem, Icon as NBaseIcon, Thumbnail, Right } from 'native-base';
import Style from '../assets/Style';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions/UserAction';
import Spinner from 'react-native-spinkit';
import CodeInput from 'react-native-confirmation-code-input';

class VerifyComponent extends Component {  
    constructor(props) {
        super(props);
        this.state = {
            number: ""
        }
    }

    verifyCode = (code) => {
        console.warn("component id: ",this.props.userReducer);
        const verificationId = this.props.userReducer.authData.verificationId;
        this.props.SignIn (verificationId, code)
    }

    render() {
        return (
            <View style={[ Style.flex, Style.paddingDefault ]}> 
                <View style={[ Style.flex ]}>
                </View>
                
                <View style={[ Style.flex, Style.justifyCenter, Style.alignCenter ]}>
                    <View style={[ Style.p5, Style.width90, Style.selfCenter ]}>
                        <Text style={[ Style.textWhite, Style.textCenter, Style.textWeighted, Style.f20 ]}>Please enter the six digit verification code sent to your number</Text>
                    </View>
                    <View style={[ Style.flexRow ]}>
                        <CodeInput
                            ref="codeInputRef1"
                            className={'border-circle'}
                            inactiveColor="rgba(255,255,255,0.5)"
                            activeColor="rgba(255,255,255,1)"
                            space={10}
                            size={30}
                            inputPosition='center'
                            keyboardType="numeric"
                            codeLength={6}
                            autoFocus={false}
                            codeInputStyle={[ Style.textBold, Style.borderWidth2 ]}
                            onFulfill={(code) => this.verifyCode(code)}
                        />
                    </View>                    
                </View>

                <View style={[Style.flex]}> 
                </View>

            </View>
        );
    }
}
function mapStateToProps(state, props) {
    return {
        userReducer: state.UserReducer
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch)
}
  
export default connect(mapStateToProps, mapDispatchToProps)(VerifyComponent);