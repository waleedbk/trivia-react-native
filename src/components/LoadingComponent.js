import React, {Component} from 'react';
import {BackHandler, ActivityIndicator, Platform, Styleheet, View, TextInput, TouchableOpacity, ImageBackground, Alert, ScrollView} from 'react-native';
import {Container, Content, Button, Badge, Text, List, ListItem, Icon as NBaseIcon, Thumbnail, Right} from 'native-base';
import Style from '../assets/Style';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions/UserAction';
import Spinner from 'react-native-spinkit';

import LottieView from "lottie-react-native";

class LoadingComponent extends Component {  
    constructor(props) {
        super(props);
        this.state = {
            tips: ["Tip: Want to suggest something intresting for the next quiz? Go to the help tab and submit trivia by filling the form.", "This is all about the real money. Yes the real one, you can get through easy paisa", "We don't eliminate anyone, You're always in the match", "Your negative match points shall not be added to the weekly points"],
            tipIndex: 1,
        }
    }

    componentDidMount() {
        setTimeout (() => {
            this.props.AuthenticateUser ();
        }, 1000);
    }

    /* Used for chaning tip */
    changeTip = () => {
        let num = this.genRandNum();
        this.setState({
            tipIndex: num
        })
    }

    /* Generates random number */
    genRandNum = () => {
        let tipsLength = this.state.tips.length;
        let tipIndex = this.state.tipIndex;
        let randomNum = Math.floor((Math.random() * tipsLength) + 0);
        if(randomNum == tipIndex) {
            return this.genRandNum();
        }
        return randomNum;
    }

    render() {
        const {tips, tipIndex} = this.state;
        return (
            <View style={[ Style.flex, Style.paddingDefault ]}> 
                <View style={Style.flex}>
                </View>
                <View style={[ Style.flex, Style.absoluteCenter ]}>
                    
                    <LottieView
                        style={[ Style.width20 ]}
                        source={require('../../public/animations/material_wave_loading.json')}
                        autoPlay
                        loop={true}
                    />                           
                    <Text style={[ Style.textWhite, Style.textBold, Style.f30, Style.marginT30 ]}>
                        Loading
                    </Text>

                </View>
                <View style={[ Style.flex, Style.alignCenter, Style.justifyBottom ]}>
                    <TouchableOpacity style={[ Style.padding15, Style.borderRadius10, Style.bgWhite ]} onPress={() => this.changeTip()}>
                        <Text style={[ Style.textBold, Style.textCenter, Style.textPrimary ]}> { tips[tipIndex] } </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
function mapStateToProps(state, props) {
    return {
        userReducer: state.UserReducer
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch)
}
  
export default connect(mapStateToProps, mapDispatchToProps)(LoadingComponent);