import React, {Component} from 'react';
import {BackHandler, ActivityIndicator, Platform, Styleheet, View, TextInput, TouchableOpacity, ImageBackground, Alert, ScrollView, Switch} from 'react-native';
import {Container, Content, Button, Badge, Text, List, ListItem, Icon as NBaseIcon, Thumbnail} from 'native-base';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FontAweIcon from 'react-native-vector-icons/FontAwesome';
import Style from '../assets/Style';
import firebase, { RNFirebase } from 'react-native-firebase';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PopupDialog, { SlideAnimation, DialogTitle, FadeAnimation } from 'react-native-popup-dialog';
import { Navigation } from  'react-native-navigation';
import * as Actions from '../actions/UserAction';
import QuizReducer from '../reducers/QuizReducer';
import { PushGameplayComponent, PushLeaderboardComponent, PushSettingComponent } from "../config/Navigation";
import LottieView from "lottie-react-native";


class HomeComponent extends Component {  
    constructor (props )
    {
        super (props);
        this.state = {
            countDown: 10,
            coolDown: 10,
            isStarted: false,
            eliminated: false,
            canPlay: 0, // 0 => can't play, 1 => play, 2 => spectate
            answers: [],
            winningPopup: false,
            showAnswer: 0,
        }
    }

    componentDidMount () 
    {
        // Back button handler
        // BackHandler.addEventListener ('hardwareBackPress', this.handleBackPress);

        // this.props.SignInAnonymously ();
        this.props.FetchNextGame ();

    }

    componentWillUnmount ()
    {
        // BackHandler.exitApp()
    }

    startGame = () =>
    {
        PushGameplayComponent();
    }

    handleBackPress = () => {
        console.warn('back pressed');
        if (this.state.isStarted == true) {
            Alert.alert(
                'Stop quiz',
                'Are you sure?', [{
                    text: 'Cancel',
                    onPress: () => console.log ('Cancel Pressed'),
                    style: 'cancel'
                }, {
                    text: 'OK',
                    onPress: () => this.resetStates()
                }, ], {
                    cancelable: false
                }
            )
            return true;
        } else {            
            Alert.alert(
                'Exit App',
                'Exiting the application?', [{
                    text: 'Cancel',
                    onPress: () => Navigation.push('LoadingComponent', {
                        component: {
                          name: 'LoadingComponent',
                          passProps: {
                            text: 'Some props that we are passing'
                          },
                          options: {
                            topBar: {
                              title: {
                                text: 'Post1'
                              }
                            }
                          }
                        }
                      }),
                    style: 'cancel'
                }, {
                    text: 'OK',
                    onPress: () => BackHandler.exitApp()
                }, ], {
                    cancelable: false
                }
            )
            return true;
        }
    }

    /** Button Event Handler */
    buttonPressed = (buttonName) => {
        switch (buttonName) {
            case "setting": {
                PushSettingComponent ();
            }
            break;
        }
    }
    /** End */

    render() {

        const { canPlay, winningPopup } = this.state;
        const { isLive, startTime, game } = this.props.quizReducer.currentGame;

        return (
            <View style={[ Style.flex, Style.paddingDefault ]}>

                <View style={[Style.flexRow, Style.height50 ]}>
                    <Text style={[ Style.f20, Style.textWhite, Style.textBold ]}>TriviaTelenor</Text>
                    <TouchableOpacity style={[ Style.floatRight]}>
                        <Icon style={Style.textWhite} name="help" size={35} />
                    </TouchableOpacity>
                </View>

                <View style={[ Style.flex ]}>
                    <View style={[ Style.flexColumn, ]}>
                        <Text style={[ Style.textCenter, Style.f20, Style.textWhite, Style.textWeighted ]}>{ ( isLive == "true" ) ? 'Game Is Live' : 'Next Game' }</Text>
                        { ( isLive == "true" ) ?
                            <TouchableOpacity style={[ Style.startButton, Style.marginV10 ]} onPress={() => this.startGame()}>
                                <Text style={[ Style.f20, Style.textBold, Style.textWhite ]}> Start </Text>
                            </TouchableOpacity>
                        :                 
                            <Text style={[ Style.marginV5, Style.f25, Style.textWhite, Style.textBold, Style.textCenter ]}>{ new Date(game.startTime).toString() }</Text>
                        }       
                        <Text style={[ Style.textCenter, Style.f25, Style.textWeighted, Style.textYellow ]}>{ (game.gamePrice != "") ? "pkr "+game.gamePrice : "" }</Text>                  
                    </View>                    

                    <View style={[ Style.flex, Style.bgWhite, Style.borderRadius10, Style.marginV20 ]}>
                        
                        <View style={[ Style.flexRow, Style.paddingDefault ]}>
                            <TouchableOpacity style={ Style.flex }></TouchableOpacity>

                            <TouchableOpacity style={[ Style.flex, Style.alignCenter ]}>
                                
                                <Thumbnail source={require('../../public/images/avatar.png')} />                       
                                 
                                <Text style={[Style.textBold, Style.textGray, Style.textCenter, Style.f20]}>John Doe</Text>
                            </TouchableOpacity>
                            
                            <TouchableOpacity style={[ Style.flex, Style.alignEnd ]} onPress={ () => this.buttonPressed('setting') }>
                                <Icon name='settings' size={25}></Icon>
                            </TouchableOpacity>
                        </View>

                        <View style={[ Style.flex, Style.flexRow, Style.alignCenter ]}>    
                            <TouchableOpacity style={Style.textBlock}>
                                <Text style={[Style.textGray, Style.textBold]}>Balance</Text>
                                <Text style={[Style.textGray]}>Rs. 5000</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={Style.textBlock}>
                                <Text style={[Style.textGray, Style.textBold]}>Points</Text>
                                <Text style={[Style.textGray]}>13400</Text>
                            </TouchableOpacity>
                        </View>

                    </View>

                </View>

                <View style={[ Style.flexRow, Style.justifySpaceBetween ]}>
                    <TouchableOpacity style={{backgroundColor: '#FF5722', width: '48%', borderRadius: 50, height: 35, flexDirection: 'row', justifyContent: 'center', alignItems: 'center',}} onPress={() => PushLeaderboardComponent()}>
                        <NBaseIcon style={{color: 'white', marginRight: '2%' }} name="star-half" />
                        <Text style={[Style.textWhite, Style.textCenter, Style.textBold]}>Leaderboard</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{backgroundColor: '#FFC107', width: '48%', borderRadius: 50, height: 35, flexDirection: 'row', justifyContent: 'center', alignItems: "center",}}>                
                        <Text style={[Style.textWhite, Style.textCenter, Style.textBold]}>Invite</Text>
                    </TouchableOpacity>
                </View>
                
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        userReducer: state.UserReducer,
        quizReducer: state.QuizReducer
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch)
}
  
export default connect(mapStateToProps, mapDispatchToProps)(HomeComponent);