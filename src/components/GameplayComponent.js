import React, {Component} from 'react';
import { BackHandler, ActivityIndicator, Platform, Styleheet, View, TextInput, TouchableOpacity, ImageBackground, Alert, ScrollView, StatusBar, Animated, Easing } from 'react-native';
import {Container, Content, Button, Badge, Text, List, ListItem, Icon as NBaseIcon, Thumbnail} from 'native-base';
import MatIcon from 'react-native-vector-icons/MaterialIcons';
import FontAweIcon from 'react-native-vector-icons/Entypo';
import Style from '../assets/Style';
import { bindActionCreators } from 'redux';
import PopupDialog, { SlideAnimation, DialogTitle, FadeAnimation } from 'react-native-popup-dialog';
import { Navigation } from  'react-native-navigation';
import AnimatedBar from "react-native-animated-bar";
import { SetHomeComponent } from "../config/Navigation";
import LottieView from "lottie-react-native";

import { connect } from 'react-redux';
import * as Actions from '../actions/QuizAction';
import UserReducer from '../reducers/UserReducer';

class GameplayComponent extends Component {  
    constructor (props)
    {
        super(props);
        this.state = {
            selected: false,    
            selectedOption: [],
            coolDown: 10,
            showAnswer: 0,
            deltaTime: 0,
        }

        this.animatedValue = new Animated.Value(0);
    }

    componentDidMount ()
    {
        this.props.StartGame (this.props.userReducer.userProfile.uid);
        this.startGame ();

        const serverTime = this.props.quizReducer.currentGame.serverTime;
        const deltaTime = this.state.serverTime - new Date().getTime();

        this.setState ({
            deltaTime: deltaTime,
        })
        console.warn ( "client Time", new Date().getTime(), "server time ", serverTime, "delta ", serverTime - new Date().getTime() );
    }

    componentWillMount ()
    {
        clearInterval (this.countDownInterval);
    }

    animateBackgroundColorUp = () =>
    {
        this.animatedValue.setValue(0);
        Animated.timing (
            this.animatedValue,
            {
                toValue: 10,
                duration: 5000,           
                easing: Easing.linear,    
            }
        ).start( (o) => { if (o.finished) { this.animateBackgroundColorDown() } } );
    }


    animateBackgroundColorDown = () =>
    {
        this.animatedValue.setValue(0);
        Animated.timing (
            this.animatedValue,
            {
                toValue: 1,
                duration: 5000,    
                easing: Easing.linear,    
            }
            ).start( (o) => { if (o.finished) { this.animateBackgroundColorUp() } } );
    }


    resetStates = () => {
        clearInterval (this.countDownInterval);
        this.props.ResetGameState();
        this.setState((prevState) => ({
            coolDown: 10,
        }));
    }

    startGame = () => {
        // this.startCountDown();
    }

    endGame = () =>
    {
        SetHomeComponent();
        this.props.EndGame();
    }

    startCountDown = () => {
        this.countDownInterval = setInterval(() => {
            this.decrementCountDown();
        }, 1000);
    }

    decrementCountDown = () => {

        if (this.state.coolDown > 0)
        {
            this.setState ((prevState) =>
            ({
                coolDown: prevState.coolDown-1,
            }) )
        }

        // if(this.state.coolDown == 0)
        // {    
        //     if ( this.props.quizReducer.currentQuiz.currentQuizIndex < this.props.quizReducer.currentQuiz.quiz.length - 1 )
        //     {
        //         clearInterval (this.countDownInterval);

        //         this.props.ShowAnswer (1);

        //         setTimeout(() => {
        //             this.props.IncCurrentIndex ();
        //             this.setState ((prevState) => ({
        //                 coolDown: 10,
        //             }))
        //             this.startCountDown();

        //             this.props.ShowAnswer(0);
        //         }, 3000);
        //     } else
        //     {
        //         clearInterval (this.countDownInterval);
        //         this.props.ShowAnswer (1);
        //         setTimeout( () =>
        //         {
        //             this.popupDialog.show()
        //         }, 2000 )
        //     }
        // } else
        // {
        //     this.setState( (prevState) => 
        //     ({
        //         coolDown: prevState.coolDown-1,
        //     }) )
        // }
    }

    selectOption = (currentQuizIndex, optionIndex, confidenceLevel) =>
    {
        this.animateBackgroundColorUp();
        // this.setState( prevState => ({
        //     selectedOption: {
        //         [ currentQuizIndex ]: [ optionIndex, confidenceLevel ]
        //     }
        // }))
        // console.warn (optionIndex, confidenceLevel);

        /** Calling SelectOption action */
        if (this.props.quizReducer.currentGame.showAnswer == "false")
        {
            console.warn("answer selected")
            this.props.SelectOption ( optionIndex, confidenceLevel );
        }

        if (this.state.selected == false) 
        {
            this.setState({
                selected: true
            });
        } else
        {
            this.setState({
                selected: false
            })
        }
    }

    renderQuiz = () => 
    {
        const backgroundColorVar = this.animatedValue.interpolate (
            {
                inputRange: [ 1, 10 ],
                outputRange: [ '#ff7043', '#ff5722']
            } );

        const quiz = this.props.quizReducer;
        const currentQuiz = this.props.quizReducer.currentGame;
        const currentQuizIndex = this.props.quizReducer.currentGame.currentIndex;
        const currentQuestion = this.props.quizReducer.currentGame.game.quiz[ currentQuizIndex ];
        const totalPoints = this.props.quizReducer.currentGame.game.totalPoints;
        const pointsScored = this.props.quizReducer.pointsScored;

        const answers = [];

        for (let i = 0; i < currentQuestion.options.length; i++) {
            answers.push(
                <View style={[ Style.flexRow, Style.marginV5, Style.borderRadius50, Style.borderLight ]} key={i}>
                    <Text style={[ Style.padding20, Style.zIndex ]} pointerEvents="none">
                        {
                            currentQuestion.options[i]
                        }
                    </Text>

                    <View style={[ Style.positionAbsolute, Style.flex, Style.height100, Style.flexRow, Style.width100, Style.borderRadius50,
                        (quiz.selectedOption[ currentQuizIndex ] != null && (currentQuiz.showAnswer  == "true")) ? (i == currentQuestion.correct) ? Style.bgGreen : ( quiz.selectedOption[currentQuizIndex][ 0 ] == i ) ? Style.bgRed : "" : (currentQuiz.showAnswer  == "true" && (i == currentQuestion.correct)) ? Style.bgGreen : ""
                    ]}>
                        <TouchableOpacity style={[ Style.flex, Style.height100, Style.borderLeftRounded,  ( quiz.selectedOption[ currentQuizIndex ] != null ) ?  ( quiz.selectedOption[currentQuizIndex][0] == i ) ? ( currentQuiz.showAnswer == "true" ) ? (quiz.selectedOption[ currentQuizIndex ][ 0 ] == currentQuestion.correct) ? Style.bgGreen : Style.bgRed : (quiz.selectedOption[currentQuizIndex][1] >= 1 ) ? {backgroundColor:backgroundColorVar} : "" : "" : "" ]}>
                        </TouchableOpacity>
                        <TouchableOpacity style={[ Style.flex, Style.height100, Style.borderLeft, Style.borderRight, ( quiz.selectedOption[ currentQuizIndex ] != null ) ?  ( quiz.selectedOption[currentQuizIndex][0] == i ) ? ( currentQuiz.showAnswer == "true"  ) ? (quiz.selectedOption[ currentQuizIndex ][ 0 ] == currentQuestion.correct) ? Style.bgGreen : Style.bgRed : (quiz.selectedOption[currentQuizIndex][1] >= 2 ) ? {backgroundColor:backgroundColorVar} : "" : "" : "" ]}>
                        </TouchableOpacity>
                        <TouchableOpacity style={[ Style.flex, Style.height100, Style.borderRightRounded, ( quiz.selectedOption[ currentQuizIndex ] != null ) ?  ( quiz.selectedOption[currentQuizIndex][0] == i ) ? ( currentQuiz.showAnswer == "true" ) ? (quiz.selectedOption[ currentQuizIndex ][ 0 ] == currentQuestion.correct) ? Style.bgGreen : Style.bgRed :  (quiz.selectedOption[currentQuizIndex][1] == 3) ? {backgroundColor:backgroundColorVar} : "" : "" : "" ]}>
                        </TouchableOpacity>
                    </View>

                    <View style={[ Style.positionAbsolute, Style.flex, Style.height100, Style.flexRow, Style.width100, Style.zIndex2 ]}>
                        <TouchableOpacity style={[ Style.flex, Style.height100 ]} onPress={() => this.selectOption(currentQuizIndex, i, 1)}>
                        </TouchableOpacity>
                        <TouchableOpacity style={[ Style.flex, Style.height100 ]} onPress={() => this.selectOption(currentQuizIndex, i, 2)}>
                        </TouchableOpacity>
                        <TouchableOpacity style={[ Style.flex, Style.height100 ]} onPress={() => this.selectOption(currentQuizIndex, i, 3)}>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }

        return (
            <View style={[ Style.bgWhite ]}>

                <View style={Style.flexRow}>
                    <View style={[ Style.flex, Style.flexRow, Style.alignCenter ]}>
                        <MatIcon name='person' size={20} color={"#bdbdbd"}></MatIcon>
                        <Text style={[ Style.textLight, Style.textBold ]}> 
                            12
                        </Text>
                    </View>
                    <View style={[ Style.padding20, Style.bgLight, Style.borderRadius50 ]}>
                        <View style={[ Style.absoluteCenter ]}>
                            <Text style={[ Style.textBold, Style.textPrimary ]}>{ this.props.quizReducer.currentGame.countDown }</Text>
                        </View>
                    </View>
                    <View style={[ Style.flex, Style.flexRow, Style.alignCenter ]}>
                        <AnimatedBar 
                            height={15}
                            borderColor="#eeeeee"
                            fillColor="white"
                            barColor="green"
                            borderWidth={2}
                            progress={this.props.quizReducer.progress} 
                            width={100}
                            duration={500}
                            row
                        />                
                        <Text style={[ Style.f15, Style.textLight, Style.textBold ]}> { pointsScored } / { totalPoints } </Text>
                        {(quiz.animation.type == "progressPlus") ?
                            <LottieView
                                style={[ Style.positionAbsolute, Style.selfCenter, ]}
                                source={require('../../public/animations/star.json')}
                                autoPlay
                                loop={false}
                                
                            />
                            :
                            <View></View>
                        }
                    </View>
                </View>

                <View style={[ Style.marginV5 ]}>
                    <View style={[ Style.marginV25 ]}>
                        <Text style={[ Style.textPrimary, Style.f20, Style.textBold, Style.textCenter ]}> { currentQuestion.question } </Text>
                    </View>
                    {answers}
                </View>
                
            </View>
        )
    }

    render() 
    {

        const { isGameStarted, pointsScored, currentGame, animation } = this.props.quizReducer;
        
        return (
            <View style={[ Style.flex, Style.paddingDefault ]}>
                
                { ( !isGameStarted ) ?
                    <View style={[ Style.paddingDefault, Style.bgWhite, Style.borderRadius10 ]}>
                    
                    </View>
                   :
                   
                    <View style={[ Style.paddingDefault, Style.bgWhite, Style.borderRadius10 ]}>
                        
                        { this.renderQuiz() }

                        {(animation.type == "progressPlus") ?
                            
                            <LottieView
                                style={[ Style.positionAbsolute, Style.selfCenter, Style.flex, Style.height100, Style.width100 ]}
                                source={require('../../public/animations/confetti.json')}
                                autoPlay
                                loop={false}
                            />
                            :
                            <View></View>
                        }

                        <PopupDialog width={0.8} height={0.6} show={(currentGame.showResults == "true") ? true : false} containerStyle={{zIndex: 10}} ref={(popupDialog) => { this.popupDialog = popupDialog; }} dialogAnimation={this.slideAnimation} onDismissed={() => this.endGame()}>
                            <View style={Style.popupContainer}>
                                <Text style={[Style.textCenter, Style.textBold, Style.f25]}>You have completed!</Text>
                                <View style={Style.popupBody}>
                                    <Text style={[Style.textBold, Style.textMuted, Style.textCenter, Style.f20]}>Points scored { pointsScored } out of { currentGame.game.totalPoints }</Text>
                                    <Text style={[Style.textMuted, Style.textCenter, Style.f20]}>Congratulations!</Text>
                                    <Text style={[Style.textMuted, Style.textCenter, Style.f20]}>Tell your friends!</Text>
                                    <TouchableOpacity rounded iconLeft style={{marginTop: 15, backgroundColor: '#FFEB3B', alignSelf: 'center', paddingHorizontal: 30, paddingVertical: 5, borderRadius: 50}}>                
                                        <Text style={[Style.textBlack, Style.f20]}>Share</Text>
                                    </TouchableOpacity>
                                </View>               
                                <Text style={[Style.textMuted, Style.textCenter, Style.f20]} onPress={() => this.endGame()}>Back To Menu</Text>         
                            </View>
                        </PopupDialog>

                    </View>
                } 
                
            </View>
        )
    }
}

function mapStateToProps( state, props ) {
    return {
        quizReducer: state.QuizReducer,
        userReducer: state.UserReducer
    }
}
function mapDispatchToProps( dispatch ) {
    return bindActionCreators( Actions, dispatch )
}
  
export default connect( mapStateToProps, mapDispatchToProps )( GameplayComponent );