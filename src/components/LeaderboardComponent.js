import React, {Component} from 'react';
import {BackHandler, ActivityIndicator, Platform, Styleheet, View, TextInput, TouchableOpacity, ImageBackground, Alert, ScrollView} from 'react-native';
import {Container, Content, Button, Badge, Text, List, ListItem, Icon as NBaseIcon, Thumbnail, Right, Left, Center,Switch} from 'native-base';
import Style from '../assets/Style';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions/UserAction';
import Spinner from 'react-native-spinkit';
import CodeInput from 'react-native-confirmation-code-input';
import { Navigation } from 'react-native-navigation';

class LeaderboardComponent extends Component {  
    constructor (props)
    {
        super(props);
        this.state = {
            number: ""
        }
    }

    componentDidMount () 
    {
        this.props.FetchLeaderboard();
    }

    authenticateUser = (code) =>
    {
        console.warn("component id: ",this.props.componentId);
        return Navigation.setStackRoot(this.props.componentId, {
            component: {
                name: 'HomeComponent',
                passProps: {
                    text: 'Root screen'
                },
                options: {
                    animations: {
                        setStackRoot: {
                        enable: true
                        }
                    }
                }
            }
        });
    }

    renderLeaderboardList = () => 
    {
        const leaderboard = this.props.userReducer.leaderboard;

        const list = [];
        count = 1;

        if (Object.keys(leaderboard).length > 0)
        {
            for (let key in leaderboard)
            {
                list.push (
                    <ListItem>
                        <Text style={[ Style.textGray ]}>-{ count }  </Text>
                        <Left>
                            <Text numberOfLines={1}>{ key }</Text>
                        </Left>
                        <Right>
                            <Text> { leaderboard[key].totalScore }</Text>
                        </Right>
                    </ListItem>
                )
                count++;
            }
        }

        return list;
    }

    render () {
        return (
            <View style={[ Style.flex, Style.paddingH10, Style.bgWhite ]}>                 

                <View style={[ Style.flex ]}>
                    <ScrollView>

                        <View style={[ Style.flexRow, Style.justifySpaceBetween, Style.paddingV10 ]}>
                            <TouchableOpacity style={{ backgroundColor: '#FF5722', width: '48%', borderRadius: 50, height: 40, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                <Text style={[ Style.textWhite, Style.textCenter, Style.textBold]}>Weekly</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ backgroundColor: '#FF5722', width: '48%', borderRadius: 50, height: 40, flexDirection: 'row', justifyContent: 'center', alignItems: "center", }}>                
                                <Text style={[ Style.textWhite, Style.textCenter, Style.textBold]}>All Time</Text>
                            </TouchableOpacity>
                        </View> 

                        <View style={[ Style.flexRow, Style.padding10, Style.alignCenter ]}>

                            <View style={[ Style.flex, Style.flexColumn, Style.alignCenter ]}>
                                <TouchableOpacity style={[ ]}>
                                    <Thumbnail medium source={require('../../public/images/avatar2.png')} />
                                </TouchableOpacity>
                                <Text style={[ Style.textBold, Style.textPrimary, Style.f15, Style.paddingH25, Style.paddingV5, Style.borderRadius20 ]} numberOfLines={1}>Alex</Text>    
                            </View>

                            <View style={[ Style.flex, Style.flexColumn, Style.alignCenter ]}>
                                <TouchableOpacity style={[ ]}>
                                    <Thumbnail large source={require('../../public/images/avatar.png')} />
                                </TouchableOpacity>
                                <Text style={[ Style.textBold, Style.textPrimary, Style.f15, Style.paddingH25, Style.paddingV5, Style.borderRadius20 ]} numberOfLines={1}>John Doe</Text>    
                            </View>

                            <View style={[ Style.flex, Style.flexColumn, Style.alignCenter ]}>
                                <TouchableOpacity style={[ ]}>
                                    <Thumbnail medium source={require('../../public/images/avatar3.png')} />
                                </TouchableOpacity>
                                <Text style={[ Style.textBold, Style.textPrimary, Style.f15, Style.paddingH25, Style.paddingV5, Style.borderRadius20 ]} numberOfLines={1}>Hasni</Text>    
                            </View>

                        </View>

                        <View style={[ Style.flex]}>
                            <Content>
                                <List>
                                    { this.renderLeaderboardList() }
                                </List>
                            </Content>
                        </View>

                    </ScrollView>
                </View>

                <View style={[ Style.flexRow, Style.justifySpaceBetween, Style.alignCenter, Style.justifyCenter, Style.paddingV10 ]}>
                    
                    <View style={[ Style.flex ]}>
                        <Text style={[ Style.textGray ]}>- 200</Text>
                    </View>
 
                    <View style={[ Style.flex, Style.flexRow, Style.alignCenter, Style.justifyCenter ]}>
                        <TouchableOpacity style={[ ]}>
                            <Thumbnail small source={require('../../public/images/avatar.png')} />
                        </TouchableOpacity>
                        <Text style={[ Style.textBold, Style.textPrimary, Style.f20, Style.paddingH15, Style.borderRadius20 ]} numberOfLines={1}>John Doe</Text>
                    </View>

                    <View style={[ Style.flex ]}>
                        <View style={[ Style.alignEnd]}>
                            <Text>pkr 2500</Text>
                        </View>
                    </View>
 
                </View>  
            
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        userReducer: state.UserReducer
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch)
}
  
export default connect(mapStateToProps, mapDispatchToProps)(LeaderboardComponent);