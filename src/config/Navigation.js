import { Navigation } from  'react-native-navigation';


export const PushGameplayComponent =  () =>
{
    Navigation.push ("App", 
    {
        component: {
            name: "GameplayComponent",
        }
    });
}
export const PushVerifyComponent = () => 
{
    Navigation.push ("App",
    {
        component: {
            name: "VerifyComponent"
        }
    })
}

export const SetHomeComponent = () =>
{
    Navigation.setStackRoot ("App",
    {
        component: {
            name: "HomeComponent",
        }
    })
}
export const SetLoginComponent = () =>
{
    Navigation.setStackRoot ("App",
    {
        component: {
            name: "LoginComponent",
        }
    })
}

export const PushLeaderboardComponent = () =>
{
    Navigation.push ("App", 
    {
        component: {
            name: "LeaderboardComponent",
            options: {
                topBar: {
                    visible: true,
                    drawBehind: false,
                    title: {
                        text: "Leaderboard",
                        fontSize: 20,
                        fontWeight: "bold"
                    }
                }
            }
        }
    })
}

export const PushSettingComponent = () =>
{
    Navigation.push('App', {
        component: {
            name: "SettingComponent",
            options: {
                topBar: {
                    visible: true,
                    drawBehind: false,
                    title: {
                        text: "Settings",
                        fontSize: 20,
                        fontWeight: "bold"
                    },
                    rightButtons: {
                        id: "logout",
                        text: "Logout",
                        enabled: true,
                        disableIconTint: false,
                        color: 'red',
                        disabledColor: 'black',
                    }
                }
            }
        }
    });
}