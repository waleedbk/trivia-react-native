import { Navigation } from  'react-native-navigation';
import React, { Component } from 'react';
import { StatusBar, ImageBackground } from 'react-native';
import { Provider, connect } from "react-redux";
import Style from '../assets/Style';
/* importing components/screens */
import LoadingComponent from '../components/LoadingComponent';
import LoginComponent from '../components/LoginComponent';
import VerifyComponent from '../components/VerifyComponent';
import HomeComponent from '../components/HomeComponent';
import GameplayComponent from '../components/GameplayComponent';
import SettingComponent from '../components/SettingComponent';
import HelpComponent from '../components/HelpComponent';
import LeaderboardComponent from '../components/LeaderboardComponent';
/* *** */

function wrapComponent(component, store) {
    return class MyComponent extends Component {
        componentDidMount() {
            StatusBar.setHidden(true);
        }

        render () {
          return (
            <Provider store={store}>
                <ImageBackground style={ Style.imgBackground } resizeMode='cover' source={require('../../public/images/app-bg.png')}> 
                    {
                        React.createElement(component, {componentId: this.props.componentId})
                    }
                </ImageBackground>
            </Provider>
          )
        }
    }
}

export default function RegisterComponent(store) {
    Navigation.registerComponent('LoadingComponent', () => wrapComponent(LoadingComponent, store));
    Navigation.registerComponent('LoginComponent', () => wrapComponent(LoginComponent, store));
    Navigation.registerComponent('VerifyComponent', () => wrapComponent(VerifyComponent, store));
    Navigation.registerComponent('HomeComponent', () => wrapComponent(HomeComponent, store));
    Navigation.registerComponent('GameplayComponent', () => wrapComponent(GameplayComponent, store));
    Navigation.registerComponent('SettingComponent', () => wrapComponent(SettingComponent, store));
    Navigation.registerComponent('HelpComponent', () => wrapComponent(HelpComponent, store));
    Navigation.registerComponent('LeaderboardComponent', () => wrapComponent(LeaderboardComponent, store));
    
}