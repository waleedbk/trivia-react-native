import { StyleSheet } from "react-native";

const Style = StyleSheet.create ({
    container: {
        flex: 1,
        backgroundColor: "#303F9F",
    },
    imgBackground: {
        width: "100%",
        height: "100%",
        flex: 1 
    },
    navBar: {
        paddingHorizontal: 15,
        alignItems: "center",
        flexDirection: "row",
        height: 60,
        // backgroundColor: "transparent",
        justifyContent: "space-between",
        elevation: 3
    },
    title: {
        color: "white",
        fontSize: 20
    },
    body: {
        flex: 1,
    },
    gameTime: {
        marginHorizontal: 20,
        flexDirection: "column",
    },
    gameTitle: {
        color: "white",
        textAlign: "center",
        fontSize: 20,
        paddingBottom: 5,
    },
    gameTimeTitle: {
        color: "white",
        textAlign: "center",
        fontSize: 25,
        fontWeight: "bold",
    },

    /** ********************************************** Buttons ********************************************** */
    button: {
        paddingHorizontal: 50,
        paddingVertical: 6,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    startButton: {
        backgroundColor: "#536DFE",
        borderRadius: 50,
        alignSelf: "center",
        paddingHorizontal: 70,
        paddingVertical: 6

    },

    /** ********************************************** Buttons End ********************************************* */

    profileContainer: {
        flex:0.9,
        marginVertical: 10,
        marginHorizontal: 20,
        backgroundColor: "white",
        borderRadius: 10,
        alignSelf: "flex-end"
    },
    profileSetting: {
        flex: 1,
        alignItems: "flex-end"
    },
    profileBody: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: "center"
    },
    profileFooter: {
        paddingHorizontal: 15,
        paddingVertical: 5,
        flexDirection: "row",
    },
    textBlock: {
        height: 100,
        width: "50%",
        padding: 10,
        justifyContent: "center",
        alignItems: "center",
        marginVertical: 15,
        borderTopWidth: 1,
        borderTopColor: "#e0e0e0",
        borderStartWidth: 1,
        borderStartColor: "#e0e0e0",
        borderEndWidth: 1,
        borderEndColor: "#e0e0e0",
        borderBottomWidth: 1,
        borderBottomColor: "#e0e0e0",
    },
    bottom: {
        flexDirection: "row",
        marginHorizontal: 20,
        marginBottom: 10,
        justifyContent: "space-between",
    },

    /** ************************************** Background Colors ***************************************** */
    bgPrimary: {
        backgroundColor: "#212121"
    },
    bgSecondary: {
        backgroundColor: "#757575"
    },
    bgLight: {
        backgroundColor: "#e0e0e0",
    },
    bgDark: {
        backgroundColor: "#424242",
    },
    
    bgPurple: {
        backgroundColor: '#536DFE'
    },
    bgGreen: {
        backgroundColor: "#4CAF50",
    },
    bgWhite: {
        backgroundColor: "#ffffff",
    },
    bgRed: {
        backgroundColor: "#D32F2F"
    },
    bgYellow: {
        backgroundColor: "#fbc02d"
    },
    bgOpacity7: {
        opacity: 0.7
    },
    /** ************************************** Background Colors End ************************************** */

    /** Text Styles */
    textPrimary: {
        color: "#212121"
    },
    textSecondary: {
        color: "#757575"
    },
    textLight: {
        color: "#bdbdbd"
    },
    textDark: {
        color: "#212121"
    },

    textWhite: {
        color: "#FFFFFF"
    },
    textBlack: {
        color: "black"
    }, 
    textOrange: {
        color: "#FF5722"
    },
    textYellow: {
        color: "#fbc02d"
    },
    textMuted: {
        color: "#b0b0b0"
    },
    textGray: {
        color: "#555555"
    },
    textBold: {
        fontWeight: "bold",
    },
    textWeighted: {
        fontWeight: "600"
    },
    textCenter: {
        textAlign: "center",
    },
    f25: {
       fontSize: 25 
    },
    f15: {
        fontSize: 15 
    },
    f20: {
        fontSize: 20 
    },
    f30: {
        fontSize: 30
    },
    f50: {
        fontSize: 50
    },

    /** Padding */
    paddingDefault: {
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    padding5: {
        padding: 5,
    },
    padding10: {
        padding: 10,
    },
    padding15: {
        padding: 15,
    },
    padding20: {
        padding: 20,
    },
    paddingV5: {
        paddingVertical: 5,
    },
    paddingV10: {
        paddingVertical: 10,
    },
    paddingV15: {
        paddingVertical: 15,
    },
    paddingH5: {
        paddingHorizontal: 5,
    },
    paddingH10: {
        paddingHorizontal: 10,
    },
    paddingH15: {
        paddingHorizontal: 15,
    },
    paddingH20: {
        paddingHorizontal: 20,
    },
    paddingH25: {
        paddingHorizontal: 25,
    },

    /** End Padding */
    marginV5: {
        marginVertical: 5,
    },
    marginV7: {
        marginVertical: 7,
    },
    marginV8: {
        marginVertical: 8,
    },
    marginV10: {
        marginVertical: 10,
    },
    marginV15: {
        marginVertical: 15,
    },
    marginV20: {
        marginVertical: 20,
    },
    marginV25: {
        marginVertical: 25,
    },
    marginH5: {
        marginHorizontal: 5,
    },
    marginH10: {
        marginHorizontal: 10,
    },
    marginH15: {
        marginHorizontal: 15,
    },
    marginH20: {
        marginHorizontal: 20,
    },
    marginH25: {
        marginHorizontal: 25,
    },
    marginT10: {
        marginTop: -10,
    },
    marginT20: {
        marginTop: -20,
    },
    marginT30: {
        marginTop: -30,
    },

    floatRight: {
        marginLeft: "auto"
    },
    floatLeft: {
        marginRight: "auto"
    },
    floatCenter: {
        marginLeft: "auto",
        marginRight: "auto"
    },
    absoluteCenter: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: "center",
        alignItems: "center"
    },

    /** ************************************************* Width and Height ********************************************* */
    
    /** Width Percentage */
    
    width20: {
        width: "20%"
    },
    width30: {
        width: "30%"
    },
    width50: {
        width: "50%"
    },
    width70: {
        width: "70%"
    },
    width80: {
        width: "80%"
    },
    width90: {
        width: "90%"
    },
    width100: {
        width: "100%"
    },
    /** End Width Percentage */

    /** Height  */
    height40: {
        height: 40
    },
    height45: {
        height: 45
    },
    height50: {
        height: 50
    },
    height60: {
        height: 60
    },
    height70: {
        height: 70
    },
    height100: {
        height: "100%"
    },
    /** End Height */
    /** ************************************************* End Width And Height ******************************************* */

    /** Boders */
    borderWidth2: {
        borderWidth: 2
    },
    borderBottomWidth1: {
        borderBottomWidth: 1
    },
    borderBottomWhite: {
        borderBottomColor: "#FFFFFF"
    },
    borderRadius5: {
        borderRadius: 5
    },
    borderRadius10: {
        borderRadius: 10
    },
    borderRadius20: {
        borderRadius: 20
    },
    borderRadius25: {
        borderRadius: 25
    },
    borderRadius50: {
        borderRadius: 50
    },
    borderLeftRounded: {
        borderTopLeftRadius: 50,
        borderBottomLeftRadius: 50
    },
    borderRightRounded: {
        borderTopRightRadius: 50,
        borderBottomRightRadius: 50
    },
    borderRight: {
        borderRightWidth: 2,
        borderRightColor: "#eeeeee"
    },
    borderLeft: {
        borderLeftWidth: 2,
        borderLeftColor: "#eeeeee"
    },
    borderTop: {
        borderTopWidth: 2,
        borderTopColor: "#eeeeee"
    },
    borderLight: {
        borderWidth: 2,
        borderColor: "#eeeeee"
    },
    /** Borders End */

    popupContainer: {
        flex: 1,
        padding: 15,
        backgroundColor: "#ffffff",
        borderRadius: 50,
    },
    popupBody: {
        flex: 1,
        flexDirection: "column",
        margin: 15,
        alignItems: "center",
        justifyContent: "center",

    },

    /** ************************************ Flex/Alignment Classes *********************************** */

    flex: {
        flex: 1,
    },
    flexColumn: {
        flexDirection: "column",
    }, 
    flexRow: {
        flexDirection: "row",
    },
    alignCenter: {
        alignItems: "center",
    },
    alignEnd: {
        alignItems: "flex-end"
    },
    alignStart: {
        alignItems: "flex-start"
    },
    justifyCenter: {
        justifyContent: "center",
    },
    justifyBottom: {
        justifyContent: "flex-end"
    },

    selfCenter: {  
        alignSelf: "center",
    },

    justifySpaceBetween: {
        justifyContent: "space-between",
    },
    alignSpaceBetween: {
        alignContent: "space-between"
    },


    positionAbsolute: {
        position: "absolute"
    },

    positionRelative: {
        position: "relative"
    },
    /** ************************************ Flex/Alignment End *************************************** */
    /** Divider */
    divider: {
        borderBottomWidth: 1,
        borderBottomColor: "#eeeeee"
    },
    /** Divider End */
    
    livesText: {
        position: "absolute",
        alignSelf: "center",
        alignItems: "center",
        left: 0,
        right: 0,
    },
    numberContainer: {
        alignContent: "center"
    },
    numberInput: {
        borderBottomWidth: 1,
        borderBottomColor: "#FFFFFF",
        fontSize: 20,
    },      
    countDown: {
        flex:1,
        fontSize: 40,
        textAlign: "center",
        justifyContent: "center",
    },
    quizContainer: {
        marginHorizontal: 15,
    },
    quizQuestion: {
        fontWeight: "bold",
        fontSize: 28,
        marginBottom: 15,
    },
    quizOption: {
        padding: 15,
        borderRadius: 50,
        marginVertical: 5
    },
    zIndex: {
        zIndex: 1
    },
    zIndex2: {
        zIndex: 2
    }
});

export default Style;